import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";

import "../static/style.css";
import reducers from "./reducers";
import DetailsForm from "../src/containers/detailsForm";
import PreviewForm from "../src/containers/previewDetails";

ReactDOM.render(
  <Provider
    store={createStore(
      reducers,
      window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__()
    )}
  >
    <div className="preview-container">
      <DetailsForm />
      <PreviewForm />
    </div>
  </Provider>,
  document.querySelector(".container")
);
