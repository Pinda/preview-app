import { ADD_IMAGE } from "../actions";

export default function(state = {}, action) {
  switch (action.type) {
    case ADD_IMAGE: {
      return { ...state, image: action.payload };
    }

    default:
      return state;
  }
}
