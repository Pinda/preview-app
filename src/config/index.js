export const fieldsList = [
  { tag: "givenName", label: "Given Name", errorMessage: "Enter valid name" },
  { tag: "surname", label: "Surname", errorMessage: "Enter valid surname" },
  {
    tag: "email",
    label: "Email",
    errorMessage: "Enter valid email",
    validator: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
  },
  {
    tag: "phone",
    label: "Phone",
    errorMessage: "Enter mobile number",
    validator: /^[0]{1}[4]{1}[0-9]{8}$/,
    validatorMessage: "Invalid mobile number"
  },
  {
    tag: "houseName",
    label: "House Name",
    errorMessage: "Enter valid address"
  },
  { tag: "street", label: "Street", errorMessage: "Enter valid street" },
  { tag: "suburb", label: "Suburb", errorMessage: "Enter valid suburb" },
  { tag: "state", label: "State", errorMessage: "Enter valid state" },
  {
    tag: "postcode",
    label: "Post code",
    errorMessage: "Enter valid postcode",
    validator: /^[0-9]{4}$/,
    validatorMessage: "Enter 4 digit post code"
  },
  { tag: "country", label: "country", errorMessage: "Enter valid country" }
];
