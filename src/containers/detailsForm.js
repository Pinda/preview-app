import React, { Component } from "react";
import { reduxForm } from "redux-form";
import noop from "lodash/noop";

import { fieldsList } from "../config";
import { uploadIamge } from "../actions";

class NewPost extends Component {
  constructor() {
    super();

    this.state = { base64Url: "", validForm: false };
  }

  onSubmit() {
    this.setState({ validForm: true });
  }

  renderInputFields() {
    const { fields } = this.props;
    return fieldsList.map(value => {
      return (
        <div
          key={value.tag}
          className={`form-group form-group-${value.tag} ${
            fields[value.tag].touched && fields[value.tag].invalid
              ? "danger"
              : ""
          }`}
        >
          <label>{value.label}</label>
          <input type="text" className="form-control" {...fields[value.tag]} />
          <div className="field-error">
            {fields[value.tag].touched ? fields[value.tag].error : ""}
          </div>
        </div>
      );
    });
  }

  handleImageChange(event) {
    event.preventDefault();

    const reader = new FileReader();
    const file = event.target.files[0];

    reader.onloadend = () => {
      this.setState({ base64Url: reader.result }, () => {
        this.props.uploadIamge(this.state.base64Url);
      });
    };

    reader.readAsDataURL(file);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <form
        className="form-details-container"
        onSubmit={handleSubmit(this.onSubmit.bind(this))}
      >
        <h3>hCard Builder</h3>
        <h5>Personal Details</h5>
        <div className="personal-details">{this.renderInputFields()}</div>
        <div className="btn-wrapper">
          <div className="form-group">
            <label htmlFor="file-upload" className="btn">
              Upload Avatar
            </label>
            <input
              className="hidden fileInput"
              id="file-upload"
              type="file"
              onChange={event => this.handleImageChange(event)}
            />
          </div>
          <div className="form-group">
            <button className="btn btn-submit btn-primary" type="submit">
              Submit
            </button>
          </div>
          {this.state.validForm && (
            <div className="completion-message">
              <p>Perfect! This form is valid and ready to save</p>
            </div>
          )}
        </div>
      </form>
    );
  }
}

function validate(value) {
  return fieldsList.reduce((acc, schemaObj) => {
    if (!value[schemaObj.tag]) {
      acc[schemaObj.tag] = schemaObj.errorMessage;
    } else if (
      schemaObj.validator &&
      !schemaObj.validator.test(value[schemaObj.tag])
    ) {
      acc[schemaObj.tag] = schemaObj.validatorMessage || schemaObj.errorMessage;
    }
    return acc;
  }, {});
}

export default reduxForm(
  {
    form: "details",
    fields: fieldsList.map(field => field.tag),
    validate
  },
  noop,
  dispatch => ({ uploadIamge: file => dispatch(uploadIamge(file)) })
)(NewPost);
