import React, { Component } from "react";
import { connect } from "react-redux";
import { fieldsList } from "../config";

import { avatar } from "../../static/avatar";

class Preview extends Component {
  buildPreviewFields() {
    const { details } = this.props;

    return fieldsList.map(field => {
      return (
        <div
          key={field.tag}
          className={`form-group form-group-preview form-group-preview-${
            field.tag
          }`}
        >
          <label>{field.label}</label>
          <div>
            {details && details[field.tag] ? details[field.tag].value : ""}
          </div>
        </div>
      );
    });
  }

  renderAvatar() {
    return (
      <div className="avatar-img">
        <img alt="Avatar" src={this.props.image || avatar.image} />
      </div>
    );
  }

  render() {
    const { details } = this.props;

    return (
      <div className="preview-details-container">
        <div className="preview-details-frame">
          <h4>HCARD PREVIEW</h4>
          <div className="preview-details-header">
            <div className="full-name">
              <div className="first-name">
                {details && details.givenName ? details.givenName.value : ""}
              </div>
              <div className="last-name">
                {details && details.surname ? details.surname.value : ""}
              </div>
            </div>
            {this.renderAvatar()}
          </div>
          <div className="preview-details-wrapper">
            {this.buildPreviewFields()}
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ form: { details }, card: { image } }) => ({
  details,
  image
}))(Preview);
