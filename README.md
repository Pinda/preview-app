# Preview Details App

### Getting Started

#### Using npm

```
> npm install
> npm run start
```

#### Using yarn

```
> yarn
> yarn start
```

### Preview demo

This demo is hosted on AWS on S3 bucket.

Click [here](http://app-preview-details.s3-website-ap-southeast-2.amazonaws.com) to preview demo.
